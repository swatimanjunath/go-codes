package main

import (
	"fmt"
	"math"
)

type Elevator struct {
	totalTime, floor, totalFloorDiff int
}

func (e *Elevator) floorDiff(b, c int) int {
	i := int(math.Abs(float64(b-e.floor)) + math.Abs(float64(c-b)))
	return i
}

func (e *Elevator) time() int {
	return e.totalFloorDiff * 3
}

func (e *Elevator) print() {
	fmt.Println("Time taken for the journey is: ", e.totalTime)
}

func closestLift(number1, number2, number3, a int) int {
	if number1 <= number2 && number1 <= number3 {
		return (number1 + a)
	} else if number2 <= number1 && number2 <= number3 {
		return (number2 + a)
	} else {
		return (number3 + a)
	}
}

func main() {
	presentFloor := 6
	nextFloor := 15

	e1 := Elevator{
		floor: 1}
	e2 := Elevator{
		floor: 2}
	e3 := Elevator{
		floor: 3}
	e4 := Elevator{
		floor: 0}

	x := int(math.Abs(float64(e1.floor - presentFloor)))
	y := int(math.Abs(float64(e2.floor - presentFloor)))
	z := int(math.Abs(float64(e3.floor - presentFloor)))

	e4.floor = closestLift(x, y, z, presentFloor)

	e4.totalFloorDiff = e4.floorDiff(presentFloor, nextFloor)
	e4.totalTime = e4.time()
	e4.print()

}
